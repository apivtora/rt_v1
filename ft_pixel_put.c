/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pixel_put.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 11:32:58 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/22 15:21:05 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	ft_pixel_put(t_mlx *rt, int x, int y, int color)
{
	rt->col[y * rt->size_line + 4 * x] = color & 0xff;
	rt->col[y * rt->size_line + 4 * x + 1] = (color >> 8) & 0xff;
	rt->col[y * rt->size_line + 4 * x + 2] = (color >> 16) & 0xff;
}
