/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_dot.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 16:03:04 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 15:42:01 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vector	ft_find_dot(t_vector d, t_vector p, float t)
{
	t_vector res;

	res.x = d.x * t + p.x;
	res.y = d.y * t + p.y;
	res.z = d.z * t + p.z;
	return (res);
}

t_vector	ft_find_norm(t_vector dot, t_vector c, int r)
{
	t_vector norm;

	norm.x = (dot.x - c.x) / r;
	norm.y = (dot.y - c.y) / r;
	norm.z = (dot.z - c.z) / r;
	return (norm);
}

t_vector	ft_find_light(t_vector dot, t_vector l)
{
	t_vector	un;
	float		dist;

	dist = sqrt(pow((dot.x - l.x), 2) + pow((dot.y - l.y), 2)
			+ pow((dot.z - l.z), 2));
	un.x = (l.x - dot.x) / dist;
	un.y = (l.y - dot.y) / dist;
	un.z = (l.z - dot.z) / dist;
	return (un);
}

float		ft_length(t_vector a, t_vector b)
{
	float	length;

	length = sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2)
			+ pow((a.z - b.z), 2));
	return (length);
}
