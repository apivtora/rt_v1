/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cone_alt.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/03 10:16:40 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 12:10:27 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_cap_cone(t_cone *cone, t_vector d, t_mlx *rt)
{
	float		t;
	t_vector	dot;
	t_vector	light;

	t = -1 * ft_dot_prod(ft_v_sub(rt->p, cone->end), cone->axis)
		/ ft_dot_prod(d, cone->axis);
	if (cone->distance > t)
	{
		dot = ft_find_dot(d, rt->p, t);
		if (sqrt(pow((dot.x - cone->end.x), 2) + pow((dot.y - cone->end.y), 2)
					+ pow((dot.z - cone->end.z), 2)) <= cone->rad)
		{
			light = ft_find_light(dot, rt->l);
			cone->cos_lamb = cone->side_flag * ft_dot_prod(cone->axis, light);
			cone->cos_amb = ft_dot_prod(cone->axis,
					ft_make_unit(ft_v_sum(ft_find_light(dot, rt->p), light)));
			cone->shadow = ft_shadow(light, cone->id, dot, rt);
			cone->distance = t;
			return (1);
		}
	}
	return (0);
}

static void	ft_solve(t_mlx *rt, t_essential *all, float *tang, t_cone *cone)
{
	*tang = (1 + pow(cone->tang, 2));
	all->temp = ft_v_sub(rt->p, cone->vert);
	all->a = ft_dot_prod(all->d, all->d) -
		pow(ft_dot_prod(all->d, cone->axis), 2) * (*tang);
	all->b = 2 * (ft_dot_prod(all->d, all->temp) -
			ft_dot_prod(all->d, cone->axis)
			* ft_dot_prod(all->temp, cone->axis) * (*tang));
	all->c = ft_dot_prod(all->temp, all->temp) -
		pow(ft_dot_prod(all->temp, cone->axis), 2) * (*tang);
}

static void	ft_cone_prebody(t_essential *all, t_cone *cone)
{
	all->coef = ft_dot_prod(all->d, cone->axis) *
		all->root + ft_dot_prod(all->temp, cone->axis);
	if (all->coef < 0 || all->coef > cone->len + 2)
	{
		all->root = (-all->b + sqrt(all->b * all->b -
					4 * all->a * all->c)) / (2 * all->a);
		all->coef = ft_dot_prod(all->d, cone->axis) * all->root
			+ ft_dot_prod(all->temp, cone->axis);
	}
}

static void	ft_cone_body(t_mlx *rt, t_cone *cone, t_essential *all, float tang)
{
	all->dot = ft_find_dot(all->d, rt->p, all->root);
	all->light = ft_find_light(all->dot, rt->l);
	all->norm = ft_make_unit(ft_v_sub(ft_v_sub(all->dot, cone->vert),
			ft_v_mult(cone->axis, tang * all->coef)));
	cone->cos_lamb = ft_dot_prod(all->norm, all->light);
	if (ft_dot_prod(all->norm, ft_v_sub(rt->p, all->dot)) < 0
			&& cone->cos_lamb > 0)
		cone->cos_lamb *= -1;
	cone->cos_amb = ft_dot_prod(all->norm,
	ft_make_unit(ft_v_sum(ft_find_light(all->dot, rt->p),
			all->light)));
	cone->shadow = ft_shadow(all->light, cone->id, all->dot, rt);
	cone->distance = all->root;
	ft_cap_cone(cone, all->d, rt);
}

int			ft_cone_alt(t_mlx *rt, int x, int y, t_cone *cone)
{
	t_essential	all;
	float		tang;

	all.d.x = (x - 500);
	all.d.y = (y - 500);
	all.d.z = -500;
	ft_solve(rt, &all, &tang, cone);
	if (all.b * all.b - 4 * all.a * all.c > 0)
	{
		all.root = (-all.b - sqrt(all.b * all.b - 4 * all.a * all.c))
			/ (2 * all.a);
		if (all.root < 0)
			all.root = (-all.b + sqrt(all.b * all.b - 4 *
						all.a * all.c)) / (2 * all.a);
		ft_cone_prebody(&all, cone);
		if (all.root < 0)
			return (ft_cap_cone(cone, all.d, rt));
		if (all.coef >= 0 && all.coef <= cone->len + 2)
		{
			ft_cone_body(rt, cone, &all, tang);
			return (1);
		}
		return (ft_cap_cone(cone, all.d, rt));
	}
	return (0);
}
