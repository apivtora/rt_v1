# RT_v1 #
This is UNIT factory (school 42) studiying graphical project that basically  
implements ray tracing algorithm. It can draw only four basic primitives -  
triangle, sphere, cone, cylinder. But it uses scene file as argument so they  
can be combined to create more complicated scenes  
You can see some example maps in folder Maps/ to see how to format your input files 

##Some examples   

![exmaple 1](https://bytebucket.org/apivtora/rt_v1/raw/c7314e33f2989c96e887108217108e4455a89642/Screeenshots/Screen%20Shot%202017-10-25%20at%205.19.50%20PM.png?token=61033de31cbc00db87699f5eeef1838e0948bb34)
![exmaple 2](https://bytebucket.org/apivtora/rt_v1/raw/c7314e33f2989c96e887108217108e4455a89642/Screeenshots/Screen%20Shot%202017-10-25%20at%205.26.23%20PM.png?token=307d8d08e04f6791e5384670428ca168ffaf2f1d)
![exmaple 3](https://bytebucket.org/apivtora/rt_v1/raw/c7314e33f2989c96e887108217108e4455a89642/Screeenshots/Screen%20Shot%202017-10-25%20at%205.25.12%20PM.png?token=e1f8cc6ef647a6046e693acf9b6805d8106ff712)
![exmaple 4](https://bytebucket.org/apivtora/rt_v1/raw/c7314e33f2989c96e887108217108e4455a89642/Screeenshots/Screen%20Shot%202017-10-25%20at%205.19.35%20PM.png?token=a0615afee094fbb7e5e2b4f4377bffed936fd8aa)

### Project details ###

This project is written for macOS(OS X El Capitan)  
using MinilibX graphic library (https://github.com/abouvier/minilibx)  
and it is needed to run this project  
Use 'make' to install project  

If you are running Linux you need add -lm flag to compiling flags, as well as flags for MinilibX  
specified in its documentation. Also keycodes for Linux differ from macOS, co controls may differ  

### Disclaimer ###

This project is written according to 42's "The Norm" which specifies special rules for code like  
less then 25 lines in each function, 85 symbols in line forbidden most of libraries and functions that wasnt  
written by you, also "for", "switch case", "goto" are forbidden etc...   
You can read more at (https://ncoden.fr/datas/42/norm.pdf)  
So to achieve some conditions sometimes code is modified to the point where it bacomes not really readble  