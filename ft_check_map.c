/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_map.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 12:09:11 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 12:32:47 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	ft_check_temp(char **temp)
{
	if (!temp[0] || !temp[1] || !temp[2])
	{
		write(1, "Wrong map!\n", 12);
		exit(0);
	}
}

void	ft_line_check(char **line, int nbr)
{
	if (!line[nbr])
	{
		write(1, "Wrong map!\n", 12);
		exit(0);
	}
}
