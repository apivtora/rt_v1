/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/15 13:49:30 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 15:47:41 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

float	ft_min(float min, float nbr)
{
	if (nbr < min)
		return (min);
	return (nbr);
}

float	ft_max(float nbr, float max)
{
	if (nbr > max)
		return (max);
	return (nbr);
}
