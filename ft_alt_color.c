/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_alt_color.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/14 14:22:15 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 12:30:03 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static float	ft_rgb(t_common fig, float figure, float light)
{
	float res;

	res = ft_max(0.05 * figure + light * (0.65 * figure * ft_min(0, fig.cos_lamb) +
										  0.5 *	pow(ft_min(0, fig.cos_amb), 256) * (!fig.shadow)), 1);
	return (res);
}

int				ft_alt_color(t_common fig, int light_color)
{
	t_color figure;
	t_color light;
	t_color res;

	figure.r = (float)(fig.color >> 16) / 0xff;
	figure.g = (float)((fig.color >> 8) & 0xff) / 0xff;
	figure.b = (float)(fig.color & 0xff) / 0xff;
	light.r = (float)(light_color >> 16) / 0xff;
	light.g = (float)((light_color >> 8) & 0xff) / 0xff;
	light.b = (float)(light_color & 0xff) / 0xff;
	res.r = ft_rgb(fig, figure.r, light.r);
	res.g = ft_rgb(fig, figure.g, light.g);
	res.b = ft_rgb(fig, figure.b, light.b);
	if (fig.shadow)
	{
		res.r = 0.05 * figure.r;
		res.g = 0.05 * figure.g;
		res.b = 0.05 * figure.b;
	}
	return ((int)(res.r * 0xff) * pow(16, 4) +
			(int)(res.g * 0xff) * pow(16, 2) + res.b * 0xff);
}
