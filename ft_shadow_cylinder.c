/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shadow_cylinder.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 13:20:56 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 12:31:38 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_solve(t_vector dir, t_vector origin,
		t_cylinder *cyl, t_essential *all)
{
	all->temp = ft_v_sub(origin, cyl->start);
	all->a = ft_dot_prod(dir, dir) - pow(ft_dot_prod(dir, cyl->axis), 2);
	all->b = 2 * (ft_dot_prod(dir, all->temp) -
			ft_dot_prod(dir, cyl->axis) * ft_dot_prod(all->temp, cyl->axis));
	all->c = ft_dot_prod(all->temp, all->temp) -
		pow(ft_dot_prod(all->temp, cyl->axis), 2) - cyl->rad * cyl->rad;
}

static void	ft_little(t_vector dir, t_cylinder *cyl, t_essential *all)
{
	all->root = (-all->b - sqrt(all->b * all->b - 4 * all->a * all->c))
		/ (2 * all->a);
	all->coef = ft_dot_prod(dir, cyl->axis) *
		all->root + ft_dot_prod(all->temp, cyl->axis);
}

int			ft_shadow_cylinder(t_vector dir, t_vector origin,
		t_common *com, float dist)
{
	t_essential	all;
	t_cylinder	*cyl;

	cyl = (t_cylinder*)com;
	ft_solve(dir, origin, cyl, &all);
	if (all.b * all.b - 4 * all.a * all.c + dist - dist >= 0)
	{
		ft_little(dir, cyl, &all);
		if (all.coef <= cyl->len && all.coef >= 0 && all.root >= 0)
			return (1);
		else
		{
			all.root = -1 * ft_dot_prod(ft_v_sub(origin, cyl->start), cyl->axis)
				/ ft_dot_prod(dir, cyl->axis);
			all.dot = ft_find_dot(dir, origin, all.root);
			if (ft_length(all.dot, cyl->start) <= cyl->rad && all.root >= 0)
				return (1);
			all.root = -1 * ft_dot_prod(ft_v_sub(origin, cyl->end), cyl->axis)
				/ ft_dot_prod(dir, cyl->axis);
			all.dot = ft_find_dot(dir, origin, all.root);
			if (ft_length(all.dot, cyl->end) <= cyl->rad && all.root >= 0)
				return (1);
		}
	}
	return (0);
}
