/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 13:23:09 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 13:17:34 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_H
# define RTV1_H
# define THREADS 8
# include "get_next_line.h"
# include <limits.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include "libft/libft.h"
# include <pthread.h>

typedef struct	s_color
{
	float		r;
	float		g;
	float		b;
}				t_color;

typedef struct	s_common
{
	int			id;
	int			type;
	int			color;
	float		distance;
	float		cos_lamb;
	float		cos_amb;
	int			shadow;
}				t_common;

typedef struct	s_vector
{
	float		x;
	float		y;
	float		z;
}				t_vector;

typedef struct	s_cone
{
	int			id;
	int			type;
	int			color;
	float		distance;
	float		cos_lamb;
	float		cos_amb;
	int			shadow;
	int			len;
	t_vector	vert;
	t_vector	end;
	t_vector	axis;
	float		tang;
	float		rad;
	int			side_flag;
}				t_cone;

typedef struct	s_cylinder
{
	int			id;
	int			type;
	int			color;
	float		distance;
	float		cos_lamb;
	float		cos_amb;
	int			shadow;
	int			rad;
	int			len;
	t_vector	start;
	t_vector	end;
	t_vector	axis;
	int			side_flag;
	int			side_flag2;
}				t_cylinder;

typedef struct	s_sphere
{
	int			id;
	int			type;
	int			color;
	float		distance;
	float		cos_lamb;
	float		cos_amb;
	int			shadow;
	t_vector	center;
	int			radius;
}				t_sphere;

typedef struct	s_triangle
{
	int			id;
	int			type;
	int			color;
	float		distance;
	float		cos_lamb;
	float		cos_amb;
	int			shadow;
	t_vector	a;
	t_vector	b;
	t_vector	c;
	t_vector	norm;
	t_vector	first;
	t_vector	second;
	t_vector	third;
	t_vector	pov;
	int			side_flag;
	float		temp;
}				t_triangle;

typedef struct	s_essential
{
	t_vector	d;
	t_vector	light;
	t_vector	norm;
	t_vector	dot;
	t_vector	temp;
	float		a;
	float		b;
	float		c;
	float		root;
	float		coef;
}				t_essential;

typedef struct	s_mlx
{
	void		*mlx;
	void		*win;
	void		*img;
	char		*col;
	int			bpp;
	int			size_line;
	int			endian;
	t_vector	p;
	t_vector	l;
	int			light_color;
	void		**figures;
	int			thread;
}				t_mlx;

void			ft_pixel_put(t_mlx *rt, int x, int y, int color);
t_mlx			*ft_mlx_create(void);
float			ft_dot_prod(t_vector a, t_vector b);
t_vector		ft_v_sub(t_vector a, t_vector b);
t_vector		ft_v_sum(t_vector a, t_vector b);
t_vector		ft_find_dot(t_vector d, t_vector p, float t);
t_vector		ft_find_norm(t_vector dot, t_vector c, int r);
t_vector		ft_find_light(t_vector dot, t_vector l);
t_vector		ft_cros_prod(t_vector a, t_vector b);
int				ft_triangle(t_mlx *rt, int x, int y, t_triangle *t);
t_triangle		*ft_tr_create(t_mlx *rt, char **line, int nbr);
t_cylinder		*ft_cylinder_create(t_mlx *rt, char **line, int nbr);
t_vector		ft_make_unit(t_vector vect);
t_sphere		*ft_sphere_create(char **line, int nbr);
t_cone			*ft_cone_create(t_mlx *rt, char **line, int nbr);
t_vector		ft_v_mult(t_vector vect, float nbr);
int				ft_shadow(t_vector ray, int fig_id, t_vector dot, t_mlx *rt);
void			**ft_read_map(char *argv, t_mlx *rt);
int				ft_atoi_base(const char *str, int base);
int				ft_alt_color(t_common fig, int light_color);
float			ft_min(float min, float nbr);
float			ft_max(float nbr, float max);
void			ft_turn(t_mlx **rt, int axis, int dir);
float			ft_lightbulb(t_mlx *rt, int x, int y, t_common *max);
void			ft_turn_ox(t_mlx *rt, int dir);
void			ft_turn_oy(t_mlx *rt, int dir);
void			ft_turn_oz(t_mlx *rt, int dir);
t_mlx			**ft_mlx_copy(t_mlx *rt, int nbr, char *argv);
void			ft_mlx_copy_more(t_mlx **all_rt);
int				ft_cylinder_alt(t_mlx *rt, int x, int y, t_cylinder *cyl);
int				ft_cone_alt(t_mlx *rt, int x, int y, t_cone *cone);
t_common		*ft_write_light(t_mlx*rt, char **line);
void			ft_check_temp(char **temp);
void			ft_line_check(char **line, int nbr);
void			ft_menu(t_mlx *rt);
void			ft_free_mass(char **mass);
float			ft_length(t_vector a, t_vector b);
int				ft_top_cap(t_cylinder *cyl, t_vector d, t_mlx *rt);
int				ft_bot_cap(t_cylinder *cyl, t_vector d, t_mlx *rt);
int				ft_shadow_cylinder(t_vector dir, t_vector origin,
		t_common *com, float dist);
int				ft_shadow_cone(t_vector dir, t_vector origin,
		t_common *com, float dist);
void			ft_transform_ox(int dir, t_vector *dot);
void			ft_transform_oy(int dir, t_vector *dot);
void			ft_transform_oz(int dir, t_vector *dot);
int				ft_sphere(t_mlx *rt, int x, int y, t_sphere *sp);
void			ft_choose_figure(t_mlx *rt, int x, int y, void **test);
#endif
