/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cylinder_alt.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 16:40:56 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 15:30:40 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_cyl_cos(t_essential all, t_cylinder *cyl, t_vector l, t_mlx *rt)
{
	t_vector new_axis;
	t_vector norm;
	t_vector temp;

	new_axis = ft_v_mult(cyl->axis, all.coef);
	norm = ft_make_unit(ft_v_sub(ft_v_sub(all.dot, cyl->start), new_axis));
	temp = ft_find_light(all.dot, l);
	cyl->cos_lamb = ft_dot_prod(temp, norm);
	if (ft_dot_prod(norm, ft_v_sub(rt->p, all.dot)) < 0 &&
			cyl->cos_lamb > 0)
		cyl->cos_lamb *= -1;
	cyl->cos_amb = ft_dot_prod(norm,
				ft_make_unit(ft_v_sum(ft_find_light(all.dot, rt->p), temp)));
	cyl->shadow = ft_shadow(temp, cyl->id, all.dot, rt);
}

static void	ft_solve(t_mlx *rt, t_cylinder *cyl, t_essential *all)
{
	all->temp = ft_v_sub(rt->p, cyl->start);
	all->a = ft_dot_prod(all->d, all->d) -
		pow(ft_dot_prod(all->d, cyl->axis), 2);
	all->b = 2 * (ft_dot_prod(all->d, all->temp) -
			ft_dot_prod(all->d, cyl->axis) * ft_dot_prod(all->temp, cyl->axis));
	all->c = ft_dot_prod(all->temp, all->temp) -
		pow(ft_dot_prod(all->temp, cyl->axis), 2) - cyl->rad * cyl->rad;
}

static int	ft_cyl_body(t_mlx *rt, t_cylinder *cyl, t_essential all)
{
	all.dot = ft_find_dot(all.d, rt->p, all.root);
	all.coef = ft_dot_prod(all.d, cyl->axis) * all.root +
		ft_dot_prod(all.temp, cyl->axis);
	if (all.coef <= cyl->len && all.coef >= 0)
	{
		ft_cyl_cos(all, cyl, rt->l, rt);
		cyl->distance = all.root;
		ft_top_cap(cyl, all.d, rt);
		ft_bot_cap(cyl, all.d, rt);
		return (1);
	}
	return (0);
}

static int	ft_little(t_essential *all)
{
	all->root = (-all->b - sqrt(all->b * all->b - 4 * all->a * all->c))
			/ (2 * all->a);
	if (all->root < 0)
		all->root = (-all->b + sqrt(all->b * all->b -
					4 * all->a * all->c)) / (2 * all->a);
	if (all->root < 0)
		return (0);
	return (1);
}

int			ft_cylinder_alt(t_mlx *rt, int x, int y, t_cylinder *cyl)
{
	t_essential all;

	all.d.x = x - 500;
	all.d.y = y - 500;
	all.d.z = -500;
	ft_solve(rt, cyl, &all);
	if (all.b * all.b - 4 * all.a * all.c >= 0)
	{
		if (ft_little(&all) == 0)
			return (0);
		if (ft_cyl_body(rt, cyl, all))
			return (1);
		else
		{
			all.root = (-all.b + sqrt(all.b * all.b - 4 *
						all.a * all.c)) / (2 * all.a);
			if (all.root < 0)
				return (0);
			if (ft_cyl_body(rt, cyl, all))
				return (1);
		}
		cyl->distance = INT_MAX;
		return (ft_top_cap(cyl, all.d, rt) + ft_bot_cap(cyl, all.d, rt));
	}
	return (0);
}
