/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_copy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/26 11:20:46 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 15:51:10 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	ft_mlx_copy_more(t_mlx **all_rt)
{
	int i;

	i = 1;
	while (i < THREADS)
	{
		all_rt[i]->img = all_rt[0]->img;
		all_rt[i]->col = all_rt[0]->col;
		all_rt[i]->bpp = all_rt[0]->bpp;
		all_rt[i]->size_line = all_rt[0]->size_line;
		all_rt[i]->endian = all_rt[0]->endian;
		i++;
	}
}

t_mlx	**ft_mlx_copy(t_mlx *rt, int nbr, char *argv)
{
	int		i;
	t_mlx	**all_mlx;

	all_mlx = (t_mlx**)malloc(sizeof(t_mlx*) * nbr);
	all_mlx[0] = rt;
	i = 1;
	while (i < nbr)
	{
		all_mlx[i] = ft_mlx_create();
		all_mlx[i]->mlx = rt->mlx;
		all_mlx[i]->win = rt->win;
		all_mlx[i]->l = rt->l;
		all_mlx[i]->light_color = rt->light_color;
		all_mlx[i]->figures = ft_read_map(argv, rt);
		all_mlx[i]->thread = i;
		i++;
	}
	return (all_mlx);
}
