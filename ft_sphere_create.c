/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sphere_create.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/03 13:53:19 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/13 14:32:25 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_sphere	*ft_sphere_create(char **line, int nbr)
{
	t_sphere	*sphere;
	char		**temp;

	ft_line_check(line, 3);
	sphere = (t_sphere*)malloc(sizeof(*sphere));
	sphere->type = 1;
	if (!line[1] || !line[2] || !line[3])
	{
		write(1, "wrong map\n", 13);
		exit(0);
	}
	sphere->color = ft_atoi_base(line[1], 16);
	sphere->radius = ft_atoi(line[2]);
	temp = ft_strsplit(line[3], ',');
	ft_check_temp(temp);
	sphere->center.x = ft_atoi(temp[0]);
	sphere->center.y = ft_atoi(temp[1]);
	sphere->center.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
	sphere->shadow = 0;
	sphere->id = nbr;
	return (sphere);
}
