/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_choose_figure.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/14 11:19:13 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 12:15:49 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_res(t_mlx *rt, int x, int y, t_common *search)
{
	int res;

	res = 0;
	if (search->type == 1)
		res = ft_sphere(rt, x, y, (t_sphere*)search);
	else if (search->type == 2)
		res = ft_triangle(rt, x, y, (t_triangle*)search);
	else if (search->type == 3)
		res = ft_cylinder_alt(rt, x, y, (t_cylinder*)search);
	else if (search->type == 4)
		res = ft_cone_alt(rt, x, y, (t_cone*)search);
	return (res);
}

void		ft_choose_figure(t_mlx *rt, int x, int y, void **test)
{
	int			i;
	t_common	*max;
	t_common	*search;
	int			res;
	int			total;

	i = 0;
	max = NULL;
	res = 0;
	total = 0;
	while (test[i])
	{
		search = (t_common*)test[i];
		res = ft_res(rt, x, y, search);
		total = total + res;
		if ((!max || search->distance < max->distance) && res)
			max = search;
		i++;
	}
	if (total)
		ft_pixel_put(rt, x, y, ft_alt_color(*max, rt->light_color));
	if (ft_lightbulb(rt, x, y, max))
		ft_pixel_put(rt, x, y, rt->light_color);
}
