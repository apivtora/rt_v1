/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_turn_oy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 15:59:21 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 12:25:07 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"
#define ANGLE 3.1415 / 180 * 5 * dir

static void	ft_turn_sphere(int dir, t_common *comm, t_mlx *rt)
{
	t_sphere	*sphere;
	void		*temp;

	temp = rt;
	sphere = (t_sphere*)comm;
	ft_transform_oy(dir, &(sphere->center));
}

static void	ft_turn_tr(int dir, t_common *comm, t_mlx *rt)
{
	t_triangle *tria;

	tria = (t_triangle*)comm;
	ft_transform_oy(dir, &(tria->a));
	ft_transform_oy(dir, &(tria->b));
	ft_transform_oy(dir, &(tria->c));
	tria->norm = ft_cros_prod(ft_v_sub(tria->b, tria->a),
			ft_v_sub(tria->a, tria->c));
	tria->norm = ft_make_unit(tria->norm);
	if (ft_dot_prod(tria->norm, ft_v_sub(rt->p, tria->a)) < 0)
		tria->side_flag = -1;
	else
		tria->side_flag = 1;
	tria->temp = ft_dot_prod(tria->norm, ft_v_sub(tria->b, rt->p));
	tria->first = ft_v_sub(tria->b, tria->c);
	tria->second = ft_v_sub(tria->c, tria->a);
	tria->third = ft_v_sub(tria->a, tria->b);
}

static void	ft_turn_cyl(int dir, t_common *comm, t_mlx *rt)
{
	t_cylinder *cyl;

	cyl = (t_cylinder*)comm;
	ft_transform_oy(dir, &(cyl->start));
	ft_transform_oy(dir, &(cyl->end));
	cyl->axis = ft_make_unit(ft_v_sub(cyl->end, cyl->start));
	if (-ft_dot_prod(cyl->axis, ft_v_sub(rt->p, cyl->start)) < 0)
		cyl->side_flag = 0;
	else
		cyl->side_flag = 1;
	if (ft_dot_prod(cyl->axis, ft_v_sub(rt->p, cyl->end)) < 0)
		cyl->side_flag2 = 0;
	else
		cyl->side_flag2 = 1;
}

static void	ft_turn_cone(int dir, t_common *comm, t_mlx *rt)
{
	t_cone *cone;

	cone = (t_cone*)comm;
	ft_transform_oy(dir, &(cone->vert));
	ft_transform_oy(dir, &(cone->end));
	cone->axis = ft_make_unit(ft_v_sub(cone->end, cone->vert));
	if (ft_dot_prod(cone->axis, ft_v_sub(rt->p, cone->end)) < 0)
		cone->side_flag = 0;
	else
		cone->side_flag = 1;
}

void		ft_turn_oy(t_mlx *rt, int dir)
{
	int			i;
	void		(*ft_turn_figures[5])(int dir, t_common *com, t_mlx *rt);
	t_common	*fig;

	ft_transform_oy(dir, &(rt->l));
	ft_turn_figures[1] = ft_turn_sphere;
	ft_turn_figures[2] = ft_turn_tr;
	ft_turn_figures[3] = ft_turn_cyl;
	ft_turn_figures[4] = ft_turn_cone;
	i = 0;
	while (rt->figures[i])
	{
		fig = (t_common*)rt->figures[i];
		ft_turn_figures[fig->type](dir, fig, rt);
		i++;
	}
}
