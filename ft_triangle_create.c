/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_triangle_create.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/31 12:46:02 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/13 14:39:30 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_tr_create_body(char **line, t_triangle *res)
{
	char **temp;

	temp = ft_strsplit(line[3], ',');
	ft_check_temp(temp);
	res->a.x = ft_atoi(temp[0]);
	res->a.y = ft_atoi(temp[1]);
	res->a.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
	temp = ft_strsplit(line[4], ',');
	ft_check_temp(temp);
	res->b.x = ft_atoi(temp[0]);
	res->b.y = ft_atoi(temp[1]);
	res->b.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
	temp = ft_strsplit(line[5], ',');
	ft_check_temp(temp);
	res->c.x = ft_atoi(temp[0]);
	res->c.y = ft_atoi(temp[1]);
	res->c.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
	res->type = 2;
}

t_triangle	*ft_tr_create(t_mlx *rt, char **line, int nbr)
{
	t_triangle *res;

	ft_line_check(line, 5);
	res = (t_triangle*)malloc(sizeof(*res));
	ft_tr_create_body(line, res);
	res->color = ft_atoi_base(line[1], 16);
	res->norm = ft_cros_prod(ft_v_sub(res->b, res->a),
			ft_v_sub(res->a, res->c));
	res->norm = ft_make_unit(res->norm);
	if (ft_dot_prod(res->norm, ft_v_sub(rt->p, res->a)) < 0)
		res->side_flag = -1;
	else
		res->side_flag = 1;
	res->temp = ft_dot_prod(res->norm, ft_v_sub(res->b, rt->p));
	res->first = ft_v_sub(res->b, res->c);
	res->second = ft_v_sub(res->c, res->a);
	res->third = ft_v_sub(res->a, res->b);
	res->shadow = 0;
	res->id = nbr;
	res->pov = rt->p;
	return (res);
}
