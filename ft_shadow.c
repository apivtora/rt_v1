/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shadow.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/08 18:05:06 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 12:19:57 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"
#define FUNCT t_vector origin, t_common *com, float dist

int		ft_dist_check(t_vector dot, t_vector origin, float dist)
{
	if (ft_length(dot, origin) < dist)
		return (1);
	else
		return (0);
}

int		ft_shadow_sphere(t_vector dir, t_vector origin,
		t_common *com, float dist)
{
	t_essential	all;
	t_sphere	*sphere;

	dist = dist * 1;
	sphere = (t_sphere*)com;
	all.a = ft_dot_prod(dir, dir);
	all.temp = ft_v_sub(origin, sphere->center);
	all.b = 2 * ft_dot_prod(dir, all.temp);
	all.c = ft_dot_prod(all.temp, all.temp) - sphere->radius * sphere->radius;
	if (all.b * all.b - 4 * all.a * all.c > 0 && (-all.b - sqrt(all.b *
					all.b - 4 * all.a * all.c) / (2 * all.a)) > 0)
		return (1);
	return (0);
}

int		ft_shadow_tr(t_vector dir, t_vector origin, t_common *com, float dist)
{
	t_triangle	*tr;
	float		temp;
	t_vector	dot;
	t_vector	vect;

	tr = (t_triangle*)com;
	temp = ft_dot_prod(tr->norm, ft_v_sub(tr->b, origin))
		/ ft_dot_prod(tr->norm, dir);
	if (temp < 0)
		return (0);
	dot = ft_find_dot(dir, origin, temp);
	vect = ft_v_sub(dot, tr->c);
	if (ft_dot_prod(ft_cros_prod(tr->first, vect), tr->norm) < 0)
		return (0);
	vect = ft_v_sub(dot, tr->a);
	if (ft_dot_prod(ft_cros_prod(tr->second, vect), tr->norm) < 0)
		return (0);
	vect = ft_v_sub(dot, tr->b);
	if (ft_dot_prod(ft_cros_prod(tr->third, vect), tr->norm) < 0)
		return (0);
	return (ft_dist_check(dot, origin, dist));
}

int		ft_shadow(t_vector ray, int fig_id, t_vector dot, t_mlx *rt)
{
	int			i;
	int			(*ft_figures[5])(t_vector dir, FUNCT);
	t_common	*search;

	ft_figures[1] = ft_shadow_sphere;
	ft_figures[2] = ft_shadow_tr;
	ft_figures[3] = ft_shadow_cylinder;
	ft_figures[4] = ft_shadow_cone;
	i = 0;
	while (rt->figures[i])
	{
		search = (t_common *)rt->figures[i];
		if (fig_id != search->id)
		{
			if (ft_figures[search->type](ray, dot, search,
						ft_length(dot, rt->l)))
				return (1);
		}
		i++;
	}
	return (0);
}
