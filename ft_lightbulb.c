/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lightbulb.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/22 11:13:56 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 15:45:42 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

float	ft_lightbulb(t_mlx *rt, int x, int y, t_common *max)
{
	t_vector	d;
	t_vector	temp;
	float		a;
	float		b;
	float		c;

	d.x = (x - 500);
	d.y = (y - 500);
	d.z = -500;
	a = ft_dot_prod(d, d);
	temp = ft_v_sub(rt->p, rt->l);
	b = 2 * ft_dot_prod(d, temp);
	c = ft_dot_prod(temp, temp) - 30 * 30;
	if (b * b - 4 * a * c < 0 || (max && max->distance <
				(-b - sqrt(b * b - 4 * a * c)) / (2 * a)) ||
			(-b - sqrt(b * b - 4 * a * c)) / (2 * a) < 0)
		return (0);
	return (1);
}
