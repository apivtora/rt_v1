NAME = RTv1

FLAG = -Wall -Werror -Wextra -lmlx -framework OpenGL -framework AppKit

SRC = ft_dot_transform.c ft_sphere_create.c ft_find_dot.c ft_triangle.c\
	ft_free_mass.c ft_triangle_create.c ft_lightbulb.c ft_turn.c ft_make_unit.c\
	ft_turn_ox.c ft_alt_color.c ft_max.c ft_turn_oy.c ft_atoi_base.c ft_menu.c\
	ft_turn_oz.c ft_caps.c ft_mlx_copy.c ft_v_mult.c ft_check_map.c	ft_mlx_create.c\
	ft_write_light.c ft_choose_figure.c ft_pixel_put.c get_next_line.c ft_cone_alt.c\
	ft_read_map.c ft_cone_create.c ft_shadow.c ft_cylinder_alt.c ft_shadow_cone.c\
	main.c ft_cylinder_create.c ft_shadow_cylinder.c ft_dot_prod.c ft_sphere.c

OBJ = ft_dot_transform.o ft_sphere_create.o ft_find_dot.o ft_triangle.o\
	ft_free_mass.o ft_triangle_create.o ft_lightbulb.o ft_turn.o ft_make_unit.o\
	ft_turn_ox.o ft_alt_color.o	ft_max.o ft_turn_oy.o ft_atoi_base.o ft_menu.o\
	ft_turn_oz.o ft_caps.o ft_mlx_copy.o ft_v_mult.o ft_check_map.o	ft_mlx_create.o\
	ft_write_light.o ft_choose_figure.o ft_pixel_put.o get_next_line.o ft_cone_alt.o\
	ft_read_map.o ft_cone_create.o ft_shadow.o ft_cylinder_alt.o ft_shadow_cone.o\
	main.o ft_cylinder_create.o ft_shadow_cylinder.o ft_dot_prod.o ft_sphere.o


HEADER = rtv1.h get_next_line.h

LIB = libft/libft.a

all : $(NAME)
	@echo "done"

$(NAME) : $(OBJ)
	@echo "building ."
	@make -C libft
	@echo "building .."
	@gcc  $(OBJ) $(FLAG) $(LIB) -o $(NAME)
	@echo "building ..."

%.o: %.c
	@gcc -c -o $@ $< -Wall -Werror -Wextra 

clean:
	@make clean -C ./libft
	@rm -rf $(OBJ)

fclean: clean
	@echo "cleaning"
	@make fclean -C ./libft
	@rm -rf $(NAME)

re: fclean all
	@make re -C ./libft
