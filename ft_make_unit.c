/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_make_unit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/29 12:43:02 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 15:46:45 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vector	ft_make_unit(t_vector vect)
{
	float dist;

	dist = sqrt(pow(vect.x, 2) + pow(vect.y, 2) + pow(vect.z, 2));
	vect.x = vect.x / dist;
	vect.y = vect.y / dist;
	vect.z = vect.z / dist;
	return (vect);
}
