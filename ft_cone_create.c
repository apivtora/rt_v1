/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cone_create.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/07 11:54:54 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 13:55:14 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_cone_create_body(char **line, t_cone *res)
{
	char **temp;

	res->type = 4;
	ft_line_check(line, 4);
	res->color = ft_atoi_base(line[1], 16);
	temp = ft_strsplit(line[3], ',');
	ft_check_temp(temp);
	res->vert.x = ft_atoi(temp[0]);
	res->vert.y = ft_atoi(temp[1]);
	res->vert.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
	temp = ft_strsplit(line[4], ',');
	ft_check_temp(temp);
	res->end.x = ft_atoi(temp[0]);
	res->end.y = ft_atoi(temp[1]);
	res->end.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
	res->rad = ft_atoi(line[2]);
}

t_cone		*ft_cone_create(t_mlx *rt, char **line, int nbr)
{
	t_cone *res;

	res = (t_cone*)malloc(sizeof(*res));
	ft_cone_create_body(line, res);
	res->len = sqrt(pow((res->vert.x - res->end.x), 2)
			+ pow((res->vert.y - res->end.y), 2)
			+ pow((res->vert.z - res->end.z), 2));
	res->axis = ft_make_unit(ft_v_sub(res->end, res->vert));
	res->tang = res->rad / res->len;
	res->shadow = 0;
	res->id = nbr;
	res->distance = INT_MAX;
	if (ft_dot_prod(res->axis, ft_v_sub(rt->p, res->end)) < 0)
		res->side_flag = 0;
	else
		res->side_flag = 1;
	return (res);
}
