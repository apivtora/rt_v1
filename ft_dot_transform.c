/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dot_transform.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 15:33:39 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/13 16:32:05 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"
#define ANGLE 3.1415 / 180 * 5 * dir

void	ft_transform_ox(int dir, t_vector *dot)
{
	float temp_y;

	temp_y = dot->y - 500;
	dot->y = 500 + (temp_y) * cos(ANGLE) - dot->z * sin(ANGLE);
	dot->z = temp_y * sin(ANGLE) + dot->z * cos(ANGLE);
}

void	ft_transform_oy(int dir, t_vector *dot)
{
	float temp_x;

	temp_x = dot->x - 500;
	dot->x = 500 + (temp_x) * cos(ANGLE) + dot->z * sin(ANGLE);
	dot->z = -temp_x * sin(ANGLE) + dot->z * cos(ANGLE);
}

void	ft_transform_oz(int dir, t_vector *dot)
{
	float temp_x;
	float temp_y;

	temp_y = dot->y - 500;
	temp_x = dot->x - 500;
	dot->x = 500 + temp_x * cos(ANGLE) - temp_y * sin(ANGLE);
	dot->y = 500 + temp_x * sin(ANGLE) + temp_y * cos(ANGLE);
}
