/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shadow_cone.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 13:40:27 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 12:21:11 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_solve(t_vector dir, t_vector origin,
		t_cone *cone, t_essential *all)
{
	float tang;

	tang = (1 + pow(cone->tang, 2));
	all->temp = ft_v_sub(origin, cone->vert);
	all->a = ft_dot_prod(dir, dir) -
		pow(ft_dot_prod(dir, cone->axis), 2) * tang;
	all->b = 2 * (ft_dot_prod(dir, all->temp) - ft_dot_prod(dir, cone->axis)
			* ft_dot_prod(all->temp, cone->axis) * tang);
	all->c = ft_dot_prod(all->temp, all->temp) -
		pow(ft_dot_prod(all->temp, cone->axis), 2) * tang;
}

int			ft_shadow_cone(t_vector dir, t_vector origin,
		t_common *com, float dist)
{
	t_essential	all;
	t_cone		*cone;

	cone = (t_cone *)com;
	ft_solve(dir, origin, cone, &all);
	dist *= 1;
	if (all.b * all.b - 4 * all.a * all.c >= 0)
	{
		all.root = (-all.b - sqrt(all.b * all.b - 4 * all.a * all.c))
			/ (2 * all.a);
		all.coef = ft_dot_prod(dir, cone->axis) *
			all.root + ft_dot_prod(all.temp, cone->axis);
		if (all.coef >= 0 && all.coef <= cone->len + 2 && all.root >= 0)
			return (1);
		else
		{
			all.root = -1 * ft_dot_prod(ft_v_sub(origin, cone->end), cone->axis)
				/ ft_dot_prod(dir, cone->axis);
			all.dot = ft_find_dot(dir, origin, all.root);
			if (ft_length(all.dot, cone->end) <= cone->rad && all.root >= 0)
				return (1);
		}
	}
	return (0);
}
