/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_caps.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/12 13:57:57 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 14:31:04 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int	ft_top_cap(t_cylinder *cyl, t_vector d, t_mlx *rt)
{
	float		t;
	t_vector	dot;
	t_vector	l;

	t = -1 * ft_dot_prod(ft_v_sub(rt->p, cyl->start), cyl->axis)
		/ ft_dot_prod(d, cyl->axis);
	if (t < cyl->distance && t > 0)
	{
		dot = ft_find_dot(d, rt->p, t);
		if (sqrt(pow((dot.x - cyl->start.x), 2) + pow((dot.y - cyl->start.y), 2)
					+ pow((dot.z - cyl->start.z), 2)) <= cyl->rad)
		{
			l = ft_find_light(dot, rt->l);
			cyl->cos_lamb = cyl->side_flag * -ft_dot_prod(cyl->axis, l);
			cyl->cos_amb = -ft_dot_prod(cyl->axis,
					ft_make_unit(ft_v_sum(ft_find_light(dot, rt->p), l)));
			cyl->distance = t;
			cyl->shadow = ft_shadow(l, cyl->id, dot, rt);
			return (1);
		}
	}
	return (0);
}

int	ft_bot_cap(t_cylinder *cyl, t_vector d, t_mlx *rt)
{
	float		t;
	t_vector	dot;
	t_vector	l;

	t = -1 * ft_dot_prod(ft_v_sub(rt->p, cyl->end), cyl->axis)
		/ ft_dot_prod(d, cyl->axis);
	if (t < cyl->distance && t > 0)
	{
		dot = ft_find_dot(d, rt->p, t);
		if (sqrt(pow((dot.x - cyl->end.x), 2) + pow((dot.y - cyl->end.y), 2)
					+ pow((dot.z - cyl->end.z), 2)) <= cyl->rad)
		{
			l = ft_find_light(dot, rt->l);
			cyl->cos_lamb = cyl->side_flag2 * ft_dot_prod(cyl->axis, l);
			cyl->cos_amb = ft_dot_prod(cyl->axis,
					ft_make_unit(ft_v_sum(ft_find_light(dot, rt->p), l)));
			cyl->distance = t;
			cyl->shadow = ft_shadow(l, cyl->id, dot, rt);
			return (1);
		}
	}
	return (0);
}
