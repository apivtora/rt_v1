/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cylinder_create.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/06 12:07:56 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/12 15:36:56 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_cylinder_create_body(char **line, t_cylinder *res)
{
	char **temp;

	ft_line_check(line, 4);
	res->type = 3;
	res->color = ft_atoi_base(line[1], 16);
	res->rad = ft_atoi(line[2]);
	temp = ft_strsplit(line[3], ',');
	ft_check_temp(temp);
	res->start.x = ft_atoi(temp[0]);
	res->start.y = ft_atoi(temp[1]);
	res->start.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
	temp = ft_strsplit(line[4], ',');
	ft_check_temp(temp);
	res->end.x = ft_atoi(temp[0]);
	res->end.y = ft_atoi(temp[1]);
	res->end.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
}

t_cylinder	*ft_cylinder_create(t_mlx *rt, char **line, int nbr)
{
	t_cylinder	*res;

	res = (t_cylinder*)malloc(sizeof(*res));
	ft_cylinder_create_body(line, res);
	res->len = sqrt(pow((res->start.x - res->end.x), 2)
			+ pow((res->start.y - res->end.y), 2)
			+ pow((res->start.z - res->end.z), 2));
	res->axis = ft_make_unit(ft_v_sub(res->end, res->start));
	res->shadow = 0;
	res->distance = INT_MAX;
	res->id = nbr;
	if (-ft_dot_prod(res->axis, ft_v_sub(rt->p, res->start)) < 0)
		res->side_flag = 0;
	else
		res->side_flag = 1;
	if (ft_dot_prod(res->axis, ft_v_sub(rt->p, res->end)) < 0)
		res->side_flag2 = 0;
	else
		res->side_flag2 = 1;
	return (res);
}
