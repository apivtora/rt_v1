/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 13:22:33 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 11:45:59 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	*ft_build(void *my_rt)
{
	int		x;
	int		y;
	t_mlx	*rt;

	rt = (t_mlx*)my_rt;
	y = 0;
	while (y < 1000)
	{
		x = rt->thread;
		while (x < 1000)
		{
			ft_choose_figure(rt, x, y, rt->figures);
			x += THREADS;
		}
		y++;
	}
	return (0);
}

int		exp_hook(t_mlx **all_rt)
{
	pthread_t	pthreads[THREADS];
	int			i;

	all_rt[0]->img = mlx_new_image(all_rt[0]->mlx, 1000, 1000);
	all_rt[0]->col = mlx_get_data_addr(all_rt[0]->img, &(all_rt[0]->bpp),
			&(all_rt[0]->size_line), &(all_rt[0]->endian));
	ft_mlx_copy_more(all_rt);
	i = 1;
	while (i < THREADS)
	{
		pthread_create(&(pthreads[i]), NULL, ft_build, all_rt[i]);
		i++;
	}
	ft_build(all_rt[0]);
	i = 1;
	while (i < THREADS)
	{
		pthread_join(pthreads[i], NULL);
		i++;
	}
	mlx_put_image_to_window(all_rt[0]->mlx, all_rt[0]->win,
			all_rt[0]->img, 0, 0);
	ft_menu(all_rt[0]);
	mlx_destroy_image(all_rt[0]->mlx, all_rt[0]->img);
	return (0);
}

int		key(int keycode, t_mlx **rt)
{
	if (keycode == 53)
		exit(0);
	else if (keycode == 88)
		ft_turn(rt, 1, -1);
	else if (keycode == 86)
		ft_turn(rt, 1, 1);
	else if (keycode == 91)
		ft_turn(rt, 2, -1);
	else if (keycode == 87)
		ft_turn(rt, 2, 1);
	else if (keycode == 89)
		ft_turn(rt, 3, 1);
	else if (keycode == 92)
		ft_turn(rt, 3, -1);
	else
		return (0);
	exp_hook(rt);
	return (0);
}

int		main(int argc, char **argv)
{
	t_mlx *rt;
	t_mlx **all_rt;

	if (argc != 2)
	{
		write(1, "Use one map file as argument!\n", 31);
		return (0);
	}
	rt = ft_mlx_create();
	rt->figures = ft_read_map(argv[1], rt);
	rt->mlx = mlx_init(argv[1]);
	rt->win = mlx_new_window(rt->mlx, 1000, 1000, "RT_v1");
	all_rt = ft_mlx_copy(rt, THREADS, argv[1]);
	exp_hook(all_rt);
	mlx_hook(rt->win, 2, 5, key, all_rt);
	mlx_loop(rt->mlx);
}
