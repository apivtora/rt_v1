/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_v_mult.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/07 12:38:24 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 10:14:41 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vector	ft_v_mult(t_vector vect, float nbr)
{
	t_vector tmp_vect;

	tmp_vect.x = vect.x * nbr;
	tmp_vect.y = vect.y * nbr;
	tmp_vect.z = vect.z * nbr;
	return (tmp_vect);
}
