/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_menu.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/08 11:51:19 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 12:26:06 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	ft_menu(t_mlx *rt)
{
	mlx_string_put(rt->mlx, rt->win, 750, 50, 0x00FF8C00, "Camera rotation:");
	mlx_string_put(rt->mlx, rt->win, 715, 65, 0x00FF8C00,
			"X-axis NUM 8 / NUM 5");
	mlx_string_put(rt->mlx, rt->win, 715, 80, 0x00FF8C00,
			"Y-axis NUM 4 / NUM 6");
	mlx_string_put(rt->mlx, rt->win, 715, 95, 0x00FF8C00,
			"Z-axis NUM 7 / NUM 9");
}
