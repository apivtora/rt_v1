/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_turn.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/18 12:14:53 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/13 16:31:39 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	ft_turn(t_mlx **rt, int axis, int dir)
{
	int j;

	j = 0;
	while (j < THREADS)
	{
		if (axis == 1)
			ft_turn_oy(rt[j], dir);
		else if (axis == 2)
			ft_turn_ox(rt[j], dir);
		else if (axis == 3)
			ft_turn_oz(rt[j], dir);
		j++;
	}
}
