/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_write_light.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 18:21:33 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 10:16:58 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_common	*ft_write_light(t_mlx *rt, char **line)
{
	char **temp;

	ft_line_check(line, 3);
	rt->light_color = ft_atoi_base(line[1], 16);
	temp = ft_strsplit(line[3], ',');
	ft_check_temp(temp);
	rt->l.x = ft_atoi(temp[0]);
	rt->l.y = ft_atoi(temp[1]);
	rt->l.z = ft_atoi(temp[2]);
	ft_free_mass(temp);
	return (NULL);
}
