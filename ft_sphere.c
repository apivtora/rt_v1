/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sphere.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/14 10:19:26 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 10:37:09 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_sphere_body(t_mlx *rt, t_sphere *sp, t_essential all)
{
	all.dot = ft_find_dot(all.d, rt->p, all.root);
	all.norm = ft_find_norm(all.dot, sp->center, sp->radius);
	all.light = ft_find_light(all.dot, rt->l);
	if (ft_dot_prod(all.norm, ft_v_sub(rt->p, all.dot))
			< 0 && sp->cos_lamb > 0)
		sp->cos_lamb *= -1;
	sp->cos_lamb = ft_dot_prod(all.norm, all.light);
	sp->cos_amb = ft_dot_prod(all.norm, ft_make_unit(
				ft_v_sum(ft_find_light(all.dot, rt->p), all.light)));
	sp->distance = (-all.b - sqrt(all.b * all.b - 4 * all.a * all.c))
		/ (2 * all.a);
	sp->shadow = ft_shadow(all.light, sp->id, all.dot, rt);
}

static void	ft_solve(t_mlx *rt, t_sphere *sp, t_essential *all)
{
	all->a = ft_dot_prod(all->d, all->d);
	all->temp = ft_v_sub(rt->p, sp->center);
	all->b = 2 * ft_dot_prod(all->d, all->temp);
	all->c = ft_dot_prod(all->temp, all->temp) - sp->radius * sp->radius;
}

int			ft_sphere(t_mlx *rt, int x, int y, t_sphere *sp)
{
	t_essential all;

	all.d.x = (x - 500);
	all.d.y = (y - 500);
	all.d.z = -500;
	ft_solve(rt, sp, &all);
	if (all.b * all.b - 4 * all.a * all.c < 0)
		return (0);
	all.root = (float)(-all.b - sqrt(all.b * all.b - 4 * all.a * all.c))
		/ (2 * all.a);
	if (all.root > 0)
	{
		ft_sphere_body(rt, sp, all);
		return (1);
	}
	else
	{
		all.root = (float)(-all.b + sqrt(all.b * all.b -
					4 * all.a * all.c)) / (2 * all.a);
		if (all.root < 0)
			return (0);
		else
			sp->cos_lamb = 0;
	}
	return (1);
}
