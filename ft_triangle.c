/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_triangle.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 14:09:06 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/13 15:25:21 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_dir(int x, int y, t_essential *all)
{
	all->d.x = (x - 500);
	all->d.y = (y - 500);
	all->d.z = -500;
}

int			ft_triangle(t_mlx *rt, int x, int y, t_triangle *t)
{
	t_essential all;

	ft_dir(x, y, &all);
	all.root = t->temp / ft_dot_prod(t->norm, all.d);
	if (all.root < 0)
		return (0);
	all.dot = ft_find_dot(all.d, rt->p, all.root);
	all.temp = ft_v_sub(all.dot, t->c);
	if (ft_dot_prod(ft_cros_prod(t->first, all.temp), t->norm) < 0)
		return (0);
	all.temp = ft_v_sub(all.dot, t->a);
	if (ft_dot_prod(ft_cros_prod(t->second, all.temp), t->norm) < 0)
		return (0);
	all.temp = ft_v_sub(all.dot, t->b);
	if (ft_dot_prod(ft_cros_prod(t->third, all.temp), t->norm) < 0)
		return (0);
	t->cos_lamb = t->side_flag * ft_dot_prod(t->norm,
			ft_find_light(all.dot, rt->l));
	t->cos_amb = t->side_flag * ft_dot_prod(ft_make_unit(t->norm),
			ft_make_unit(ft_v_sum(ft_find_light(all.dot, rt->p),
					ft_find_light(all.dot, rt->l))));
	t->shadow = ft_shadow(ft_find_light(all.dot, rt->l), t->id, all.dot, rt);
	t->distance = all.root;
	return (1);
}
