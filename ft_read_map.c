/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/11 13:51:48 by apivtora          #+#    #+#             */
/*   Updated: 2017/06/14 12:32:25 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_error(void)
{
	write(1, "Wrong map!\n", 11);
	exit(0);
}

static void	**ft_give_mem(int fd, char *line)
{
	void	**res;
	int		i;

	i = 0;
	if (fd < 0)
	{
		write(1, "Invalid map file!\n", 18);
		exit(0);
	}
	while (get_next_line(fd, &(line)) > 0)
	{
		free(line);
		i++;
	}
	free(line);
	close(fd);
	res = (void**)malloc(sizeof(*res) * (i + 1));
	while (i >= 0)
	{
		res[i] = NULL;
		i--;
	}
	return (res);
}

static void	*ft_get_data(char **line, int nbr, t_mlx *rt)
{
	char *figure;
	void *res;

	res = NULL;
	if (!line)
		ft_error();
	figure = ft_strtrim(line[0]);
	if (ft_strcmp(figure, "sphere") == 0)
		res = ft_sphere_create(line, nbr);
	else if (ft_strcmp(figure, "triangle") == 0)
		res = ft_tr_create(rt, line, nbr);
	else if (ft_strcmp(figure, "cylinder") == 0)
		res = ft_cylinder_create(rt, line, nbr);
	else if (ft_strcmp(figure, "cone") == 0)
		res = ft_cone_create(rt, line, nbr);
	else if ((ft_strcmp(figure, "light") == 0))
		res = ft_write_light(rt, line);
	else
		ft_error();
	free(figure);
	ft_free_mass(line);
	return (res);
}

void		**ft_read_map(char *argv, t_mlx *rt)
{
	int		fd;
	int		i;
	void	**res;
	char	*line;

	i = 0;
	line = NULL;
	fd = open(argv, O_RDONLY);
	res = ft_give_mem(fd, line);
	fd = open(argv, O_RDONLY);
	get_next_line(fd, &(line));
	free(line);
	get_next_line(fd, &(line));
	free(line);
	while (get_next_line(fd, &(line)))
	{
		res[i] = ft_get_data(ft_strsplit(line, '|'), i, rt);
		if (!res[i])
			i--;
		free(line);
		i++;
	}
	free(line);
	close(fd);
	return (res);
}
